#contains class information for GUID of visitor
import os
import uuid

blankip = "0.0.0.0"

def makeid():
    return uuid.uuid4()

def compareIDs(id1, id2): #as strings or as uuids
    return id1 == id2

class identity(object):
    def __init__(self, newID=True, ip=blankip, thisID=makeid()): #constructor
        if newID == True:
            self.uuid = makeid()
        else:
            self.uuid = thisID
        self.ip = blankip #can be overriden on constructor declaration
        
    def saveToCSV(self):
        print(self.uuid, self.ip)

#---script to execute on module---
def testMe1(): 
    id1 = makeid()
    id2 = makeid()
    id3 = id1
    
    print(id1, id2, id3)
    print("are id1 and id2 equal?:")
    print(compareIDs(id1, id2))
    print("are id1 and id3 equal?:")
    print(compareIDs(id1, id3))

def testMe2():
    id1 = identity()
    id2 = identity()
    id1.saveToCSV()
    id2.saveToCSV()
    
#testMe1()
testMe2()
